import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory

def testdata = TestDataFactory.findTestData('Data Files/Password mismatch')

for (def row = 1; row <= testdata.getRowNumbers(); row++) {
    def name = testdata.getValue('name', row)

    def dob = testdata.getValue('dob', row)

    def email = testdata.getValue('email', row)

    def phone = testdata.getValue('phone', row)

    def password = testdata.getValue('password', row)

    def password2 = testdata.getValue('password2', row)

    WebUI.openBrowser('')

    WebUI.navigateToUrl('https://demo-app.online/daftar')

    WebUI.setText(findTestObject('Object Repository/coding_id_register/input_name'), name)

    WebUI.setText(findTestObject('Object Repository/coding_id_register/input_dob'), dob)

    WebUI.setText(findTestObject('Object Repository/coding_id_register/input_email_register'), email)

    WebUI.setText(findTestObject('Object Repository/coding_id_register/input_whatsapp'), phone)

    WebUI.setEncryptedText(findTestObject('Object Repository/coding_id_register/input_password_register'), password)

    WebUI.setEncryptedText(findTestObject('Object Repository/coding_id_register/input_password_register2'), password2)

    WebUI.check(findTestObject('Object Repository/coding_id_register/input_eula'))

    WebUI.click(findTestObject('Object Repository/coding_id_register/btn_register'))

    WebUI.verifyElementVisible(findTestObject('coding_id_register/msg_error_password_mismatch'))
}

